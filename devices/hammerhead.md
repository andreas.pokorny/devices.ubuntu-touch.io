---
codename: 'hammerhead'
name: 'Nexus 5'
comment: ''
icon: 'phone'
image: 'https://ubports.com/web/image/1475/3xnexus5%20greyback.png'
maturity: .95
---

## The Comfort of Brilliant Convergence

Are you tired of being dependent on 'The Big Two' and their services and apps? Do you want to regain control over your personal and private data on your smartphone? Install Ubuntu Touch on your Nexus 5! It keeps your Nexus 5 secure, because everything unsafe is blocked by default and the OS is virtually free of viruses and other malware that can extract your private data.

[More about convergence...](https://ubports.com/devices/nexus5-convergence).
