---
codename: 'deb'
name: 'Nexus 7 2013 LTE'
comment: 'legacy'
icon: 'tablet-landscape'
image: 'http://cloud.addictivetips.com/wp-content/uploads/2013/02/Ubuntu-Touch-Nexus-10-lockscreen.jpg'
maturity: .7
---

## The budget tablet

The Nexus 7 2013 LTE is a great tablet to use Ubuntu Touch. It's often possible to find second-hand devices at very reasonable prices.

**Note**: The [Nexus 7 2013 WiFi (flo)](/device/flo) is supported as well, but other Devices from the Nexus 7 line might not work.
